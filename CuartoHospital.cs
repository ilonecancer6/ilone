﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital
{
    class CuartoHospital
    {
        private String nombre;
        private int numeroPiso;
        private int cantidadDePacientes;
        //metodos

        public String mostrarNombrePiso()
        {
           // string mensaje = "";
            //validacion
            if (nombre == null)
            {
                return "por favor ingrese nombre del cuarto";
            }
            if (numeroPiso > 1) return "por favor ingrese piso del cuarto";
            return " cuarto: "+nombre+ "esta en el piso: "+ numeroPiso;
           
        }
        //gets, set
        public String Nombre
        {
            get { return nombre; }
            set { nombre=value;}
        }
        public int NumeroPiso
        {
            get { return numeroPiso;}
            set { numeroPiso = value; }
        }
        public int CantidadDePacientes
        {
            get { return cantidadDePacientes; }
            set { cantidadDePacientes = value; }
        }


    }
}
