﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital
{
    class Persona
    {
        private String nombres;
        private String apellidos;
        private String documento;
        private String tipo;
        private DateTime fechaNacimiento;

        // metodos , siempre en verbo
        public int calcularEdad(int anio)
        {
            int anioFechaNacimiento = fechaNacimiento.Year;
            return anio - anioFechaNacimiento;
        }
        //metodos get y set
        public String Apellido {

            get { return apellidos; }
            set { apellidos = value; }
        }
        public String Nombre
        {
            get { return nombres; }
            set { nombres = value; }
        }

        public DateTime FechaNacimiento
        {
            get { return fechaNacimiento; }
            set { fechaNacimiento = value; }
        
        }


    }
    
}
