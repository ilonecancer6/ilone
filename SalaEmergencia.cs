﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital
{
    class SalaEmergencia:CuartoHospital
    {
        private int numeroDeVentiladores;


        public string mostrarCantidadDeVentiladoresSobrantes()
        {
            int reult= numeroDeVentiladores - CantidadDePacientes;
            if (reult <= 0) return "no existesn ventiladores sobrantes" + reult * -1;
            return "la cantidad de ventiladores disponibles es: " + reult;
        }

        public int NumeroDeVentiladores
        {
            get { return numeroDeVentiladores; }
            set { numeroDeVentiladores = value; }
        }
    }
}
