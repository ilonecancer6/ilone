﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital
{
    class Empleado:Persona
    {
        private String especialidad;
        private Double sueldo;
        private int mesesTrabajados;

        public double calcularSueldoPorMesesTrabajados()
        {
            //validar datos
            if (mesesTrabajados < 1) return 0;
            if (sueldo < 0) return 0;
            return mesesTrabajados*sueldo;
        }

        public double calcularSueldoPorMesesTrabajados(double porcentajeDescuento)
        {
            //validar datos
            if (porcentajeDescuento == 0) return calcularSueldoPorMesesTrabajados();
            return calcularSueldoPorMesesTrabajados() * porcentajeDescuento;
        }


        //sobre escritura
        public override string ToString()
        {
            return Nombre +" "+ Apellido;
        }

        public Double Sueldo
        { get { return sueldo;;}
            set { sueldo = value; }
        }
        public String Especialidad
        {
            get { return especialidad; ; }
            set { especialidad = value; }
        }
        public int MesesTrabajados
        {
            get { return mesesTrabajados; ; }
            set { mesesTrabajados = value; }
        }
    }

   
}
