﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital
{
    class SalaCUI:CuartoHospital
    {
        private int numeroDeCamasCuidadosIntencivos;

        public int MostrarCantidadCamasSobrantes()
        {
          int cantidad= numeroDeCamasCuidadosIntencivos - CantidadDePacientes;
           return cantidad;
        }
        public string MostrarCantidadCamasSobrantesaviso()
        {
            int dato = MostrarCantidadCamasSobrantes();
            if (dato <= 0) return "no hay camas disponibles, faltan "+dato* -1+"para cubrir demanda";
            else return "hay " + dato + " camas disponibles";
        }

        public int NumeroDeCamasCuidadosIntencivos
        {
            get { return numeroDeCamasCuidadosIntencivos; }
            set { numeroDeCamasCuidadosIntencivos = value; }
        }

    }
}
